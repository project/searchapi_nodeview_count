<?php

namespace Drupal\searchapi_nodeview_count;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class SearchapiNodeviewCountServiceProvider.
 *
 * @package Drupal\searchapi_nodeview_count
 */
class SearchapiNodeviewCountServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('statistics.storage.node');
    $definition->setClass('Drupal\searchapi_nodeview_count\AlteredNodeStatisticsDatabaseStorage');
    $definition->setArguments([
      new Reference('database'),
      new Reference('state'),
      new Reference('request_stack'),
      new Reference('entity_type.manager'),
      new Reference('search_api.entity_datasource.tracking_manager'),
      new Reference('config.factory'),
    ]);
  }

}
