<?php

namespace Drupal\searchapi_nodeview_count;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager;
use Drupal\statistics\NodeStatisticsDatabaseStorage;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default database storage backend for statistics.
 */
class AlteredNodeStatisticsDatabaseStorage extends NodeStatisticsDatabaseStorage {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The tracking manager service.
   *
   * @var \Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager
   */
  protected $trackingManager;

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs the statistics storage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection for the node view storage.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager $tracking_manager
   *   The tracking manager service provides hook implementations on behalf of
   *   the Content Entity datasource.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   */
  public function __construct(Connection $connection, StateInterface $state, RequestStack $request_stack, EntityTypeManagerInterface $entity_manager, ContentEntityTrackingManager $tracking_manager, ConfigFactoryInterface $config) {
    parent::__construct($connection, $state, $request_stack);

    $this->entityTypeManager = $entity_manager;
    $this->trackingManager = $tracking_manager;
    $this->config = $config->get('searchapi_nodeview_count.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function recordView($id) {
    $return = parent::recordView($id);

    if ($this->config->get('reindex_node') && $views = $this->config->get('reindex_node_views')) {
      $query = $this->connection->select('node_counter', 'n');
      $query->addField('n', 'totalcount');
      $query->condition('n.nid', $id);
      $total = (int) $query->execute()->fetchField();

      // Marks the node for re-index after every fifth viewing (if $views = 5).
      if ($views > 0 && $total % $views === 0) {
        $this->reindex($id);
      }
    }

    return $return;
  }

  /**
   * Marks the node and its items changed for indexes.
   *
   * @param $id
   *   Node ID.
   *
   * @return void
   */
  public function reindex($id) {
    try {
      $storage = $this->entityTypeManager->getStorage('node');
      $entity = $storage->load($id);
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      return;
    }

    if ($entity) {
      // @see search_api_entity_update()
      // Call this hook on behalf of the Content Entity datasource.
      $this->trackingManager->entityUpdate($entity);
    }
  }

}
