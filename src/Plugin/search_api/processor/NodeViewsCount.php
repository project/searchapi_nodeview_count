<?php

namespace Drupal\searchapi_nodeview_count\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds the item's URL to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "node_viewscount",
 *   label = @Translation("node's views count field"),
 *   description = @Translation("Adds the node views count to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class NodeViewsCount extends ProcessorPluginBase {

  /**
   * The statistics service to use.
   *
   * @var \Drupal\statistics\StatisticsStorageInterface
   */
  protected $statisticStorage;

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if ($datasource
      && $datasource->getEntityTypeId()
      && $datasource->getEntityTypeId() === 'node') {
      $definition = [
        'label' => $this->t('Node Views Count'),
        'description' => $this->t('Field used to count the number of Node Views'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['node_viewscount'] = new ProcessorProperty($definition);
    }
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->statisticStorage = $container->get('statistics.storage.node');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {

    $entity = $item->getOriginalObject()->getValue();
    // Get node's user views count using statistics module.
    if ($result = $this->statisticStorage->fetchView($entity->id())) {
      $fields = $item->getFields(FALSE);
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($fields, 'entity:node', 'node_viewscount');
      foreach ($fields as $field) {
        $field->addValue($result->getTotalCount());
      }
    }
  }

}
